// Variavel global para salvar o totalHoras de exercicios
var totalHoras = 0;

// Soma horas para exibir como resultado
function somaHoras(hr) {
  totalHoras += parseInt(hr);
  alteraResultado(hr);
}

// Subtrai horas para exibir como resultado
function subtraiHoras(hr) {
  totalHoras -= parseInt(hr);
  alteraResultado(hr)
}

// Altera tfooter com total de horas de exercicios
function alteraResultado(res) {
  // guarda elementos HTML
  var tfooter = document.querySelector('.tb-log').childNodes[5],
      tdResults = document.getElementById('results');

  // Se horas maior que zero, exibe resultado da soma
  if(totalHoras > 0)
    tdResults.innerText = 'Você já fez ' + totalHoras + ' horas de exercícios';
  // Caso contrario, exibe msg padrao
  else
    tdResults.innerText = 'Você ainda não registrou exercícios.';

  // Insere td com os dados no tfooter
  tfooter.appendChild(tdResults);
}

// Define os valores para criaçao do log
function setExercicio() {
  // guarda valores dos elementos
  var tempo = document.getElementById('tempo').value,
      exercicio = document.getElementById('exercicio').value,
      data = formatDate(document.getElementById('data').value);

  // Define valor padrao para variavel tempo, caso não seja preenchida
  if(tempo === '' || tempo === '0') tempo = 1;

  somaHoras(tempo);

  criaTabela(tempo, exercicio, data);
}
// Formata data de acordo com padrao BR
function formatDate (dt) {
  var ano, mes, dia;

  // Se data for diferente de vazio
  // divide a string e guarda
  // cada valor em sua respectiva variavel
  if(dt !== ''){
    ano = dt.split('-')[0],
    mes = dt.split('-')[1],
    dia = dt.split('-')[2];
  }

  // Caso contrario cria com data atual
  else{
    var date = new Date();
    ano = date.getFullYear();
    mes = date.getMonth() + 1;
    dia = date.getDate();

    // Insere 0 no começo dos meses com 1 digito
    // pra seguir o padrao do datepicker HTML5
    if(mes < 10) mes = '0' + mes;
  }

  // Devolve a data formatada
  return dia + '/' + mes + '/' + ano;
}

// cria tabela de log
function criaTabela(tmp, ex, dt) {
  // guarda elemento tbody
  var tbody = document.querySelector('.tb-log').childNodes[3];

  // Cria elementos HTML de tabela
  trNode = document.createElement('tr'),
  tdNodeTmp = document.createElement('td'),
  tdNodeEx = document.createElement('td'),
  tdNodeDt = document.createElement('td'),
  tdBt = document.createElement('td');
  tdBt.appendChild(btDel());

  // Cria nós de texto com dados do exercicio
  var txNodeTmp = document.createTextNode(tmp),
      txNodeEx = document.createTextNode(ex),
      txNodeDt = document.createTextNode(dt);

  // Insere dados nos elementos HTML
  tdNodeTmp.appendChild(txNodeTmp);
  tdNodeTmp.setAttribute('class', 'horas');
  tdNodeTmp.innerHTML += 'h';
  tdNodeEx.appendChild(txNodeEx);
  tdNodeDt.appendChild(txNodeDt);

  trNode.appendChild(tdNodeTmp);
  trNode.appendChild(tdNodeEx);
  trNode.appendChild(tdNodeDt);
  trNode.appendChild(tdBt);

  // Insere elementos na tabela
  tbody.appendChild(trNode);
}

// Cria button deletar
// Criei esta funcao para amarrar a funcao delExercicio
// em cada botao gerado dinamicamente.
function btDel() {
  // Cria elementos HTML
  var bt = document.createElement('button'),
      i = document.createElement('i');

  // Define atributos do button
  bt.setAttribute('title', 'Excluir exercício');
  bt.setAttribute('class', 'bt bt-del');

  // Define atributos do elemento i
  i.setAttribute('class', 'fa fa-ban');

  // Insere i no button
  bt.appendChild(i);

  // Adiciona funcao ao evento click de cada botao por aqui gerado
  bt.addEventListener('click', delExercicio);

  // Devolve botao criado
  return bt;
}

// Remove exercício da tabela
function delExercicio(e) {

  // Salva na variavel o elemento usado no evento
  var target = e.target.parentElement;

  // Busca entre os elementos pai a TR
  while(target.tagName !== 'TR'){
    // Enquanto nao encontrar, sobe um nivel de parentElement
    target = target.parentElement; 

    // Recupera hora a ser subtraida do total
    if (target.tagName === 'TR') {
      subtraiHoras(target.childNodes[0].innerText.split('h')[0]);
    }
  }

  // Remove elemento pai do button (TR)
  target.parentNode.removeChild(target);
}

// Recupera botao adicionar exercicios
var btAdd = document.getElementById('bt-add');
    // Adiciona funcao ao evento click
    btAdd.addEventListener('click', setExercicio);